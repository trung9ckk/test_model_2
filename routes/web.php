<?php

use Illuminate\Support\Facades\Route;
use  App\ModelSate\InProgress;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $ticket = \App\Ticket::first();
    return view('welcome' , compact('ticket'));
//    dd(InProgress::class);
});


Route::post('/update-status', function (\Illuminate\Http\Request $request) {
    $status = $request->input('ticket');
    $a = \App\ModelSate\TicketState::find($status, \App\Ticket::first());
     \App\Ticket::first()->status->transitionTo($a);
     return redirect()->back();
})->name('update-status');
