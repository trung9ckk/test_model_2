<p>Trang thái:{{ $ticket->status }}</p>
<ul>
    <form method="post" action="{{ route('update-status') }}">
        @csrf
        <select id="cars" name="ticket">
            @foreach($ticket->status->transitionableStates() as $ticket)
                <option value="{{ $ticket }}">{{ $ticket }}</option>
            @endforeach
        </select>
        <button type="submit">chuyển trạng thái</button>
    </form>
</ul>
