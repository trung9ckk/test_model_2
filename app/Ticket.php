<?php

namespace App;

use App\ModelSate\DevReview;
use App\ModelSate\InProgress;
use App\ModelSate\NeedTest;
use App\ModelSate\Open;
use App\ModelSate\ReOpened;
use App\ModelSate\Resolved;
use App\ModelSate\Testing;
use App\ModelSate\TicketState;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStates\HasStates;

class Ticket extends Model
{
    use HasStates;
    protected $table = 'tickets';

    protected $fillable = [
        'title', 'status'
    ];


    protected function registerStates(): void
    {
        $this->addState('status', TicketState::class)
            ->default(Open::class)
            ->allowTransitions([
                [Open::class, InProgress::class],
                [Open::class, NeedTest::class],
            ])
            ->allowTransitions([
                [InProgress::class, DevReview::class],
            ])
            ->allowTransitions([
                [DevReview::class, NeedTest::class],
            ])
            ->allowTransitions([
                [NeedTest::class, Testing::class],
            ])
            ->allowTransitions([
                [Testing::class, Resolved::class],
                [Testing::class, ReOpened::class],
            ])
            ->allowTransitions([
                [ReOpened::class, InProgress::class],
                [ReOpened::class, NeedTest::class],
            ])
            ->allowTransitions([
                [Resolved::class, ReOpened::class],
            ]);
    }
}
